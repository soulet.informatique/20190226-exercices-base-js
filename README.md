JS Exercises
=================

# Exercise 1
In this exercise you must only use the variables defined in js/exercise.js
When you push the button, the exercise div should display :
"1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 17, 71"

# Exercice 2
Add a boolean variable. When you push a button, toggle the value to display or hide pair numbers (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)  

# Exercice 3
Now you can add variable as you want.
Repeat the number line "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 17, 71" 50 times, in 
the "exercise" div. The code in betweeb the two brackets of function "doExercise" must not exceed 500 characters.
